<?php

namespace Factories;

use Models\Robot;
use Models\Robot1;
use Models\Robot2;
use Classes\RobotAssembler;
use Classes\RobotWarehouse;

class RobotFactory
{
	//FACTORY RESOURCES
	private $types;
	private $drafts;
	private $warehouse;

	function __construct(...$types)
	{
		//To know which types of robots we can produce
		$this->types = $types;
		$this->warehouse = new RobotWarehouse;
	}

	public function addType($type)
	{
		//add type of robot to produce (if we have class or draft of robot)
		$this->types[$type] = $type;
	}

	public function createRobot($type, int $quantity)
	{

		//check if we can produce this type of robot
		if (!in_array($type, $this->types)) {
			throw new \Exception('The Factory not assemble this Type of Robots', 1);
		}
		//try to create one
		try{
			if (class_exists($type)) {
				//create from declared class
				$this->warehouse->placeToWarehouse($type, new $type(), $quantity);
			}else{
				//create from draft. 
				if (!empty($this->drafts[$type])) {
					$this->warehouse->placeToWarehouse($type, clone $this->drafts[$type], $quantity);
				}else{
					throw new \Exception('The Factory does`t have the draft of this Robot', 1);
				}
				
			}
		}catch(\Exception $e){
			throw new \Exception($e->getMessage(), 1);
		}
		
		return $this->warehouse->getFromWarehouse($type);
	}

	private function checkAvailability($robots)
	{
		foreach ($robots as $type => $q) {
			if ($this->warehouse->getCountOnWarehouse($type) < $q) {
				throw new \Exception("The Factory does`t have enough of this Robots ({$type}). Please create first.", 1);
			}
		}
	}


	//craft new Robots
	public function assembleRobots($newType, $robotsToAssemble)
	{
		//assemble robot from are existing

		$this->checkAvailability($robotsToAssemble);

		//clear warehous data
		$this->warehouse->removeFromWarehouse($robotsToAssemble);

		//if we have a draft of this robot return cloned
		if (!empty($this->drafts[$newType])) {
			//place to warehouse
			$this->warehouse->placeToWarehouse($robot->type, clone $this->drafts[$newType], 1);
			//return all robots of new type
			return $this->warehouse->getFromWarehouse($newType);
		}

		//create assembler for new type of robot
		$assembler = new RobotAssembler;

		//make a draft and create one. Returns new properties of known type
		list(
			$speed, 
			$height, 
			$weight
		) = $assembler->mergeRobots($robotsToAssemble);

		$type = $newType;
		$robot = new Robot(
			['speed' => $speed, 'height' => $height, 'weight' => $weight, 'type' => $type]
		);

		//place to warehouse
		$this->warehouse->placeToWarehouse($robot->type, $robot, 1);

		//create a draft of brand new robot
		$this->drafts[$newType] = $robot;

		//return all robots of new type
		return $this->warehouse->getFromWarehouse($newType);
		
	}

	public function getFromWarehouse($type)
	{
		return $this->warehouse->getFromWarehouse($type);
	}

}