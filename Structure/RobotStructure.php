<?php
//Abstract properties to implement in Robot Models. (Not allowed in Interface)
namespace Structure;

abstract class RobotStructure implements RobotInterface
{
	public static $type;
	protected $weight;
	protected $speed;
	protected $height;

}