<?php
//REquired methods of Robot Models which is necessary to implement
namespace Structure;

Interface RobotInterface
{
	public function getSpeed();
	public function getWeight();
	public function getHeight();
}