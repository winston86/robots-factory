<?php
//autoload to includes required files and makes aliases
spl_autoload_register(function ($class_name) {
	if (!class_exists($class_name)) {
		$file =  __DIR__.'/'.str_replace("\\", "/", $class_name) . '.php';
		if (file_exists($file)) { 
			require_once $file;
			class_alias($class_name, array_pop(explode("\\", $class_name)));
		}	
	}
});

//build a factory which can create some types of robots
$robotFactory = new Factories\RobotFactory(
	Models\Robot1::$type,
	Models\Robot2::$type
);

//create two types of robots
$productionOfRobot1 = $robotFactory->createRobot(Models\Robot1::$type, 5);
$productionOfRobot2 = $robotFactory->createRobot(Models\Robot2::$type, 2);

var_dump($productionOfRobot1);
var_dump($productionOfRobot2);


//Create Complex Robot
$productionOfComplexRobot = $robotFactory->assembleRobots('ComplexRobot', [
	Models\Robot1::$type => 3,
	Models\Robot2::$type => 1
]);

//if have created and type added we can use createRobot() in further
$robotFactory->addType('ComplexRobot');

foreach ($productionOfComplexRobot as $key => $robot) {
	echo 'Type: <b>'.$robot->type."</b><br>";
	echo 'Speed: '.$robot->getSpeed()."<br>";
	echo 'Weight: '.$robot->getWeight()."<br>";
	echo 'Height: '.$robot->getHeight()."<br>";
}

//view refreshed warehouse of robots
var_dump($robotFactory->getFromWarehouse(Models\Robot1::$type));
var_dump($robotFactory->getFromWarehouse(Models\Robot2::$type));