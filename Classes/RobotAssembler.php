<?php

namespace Classes;

class RobotAssembler
{

	private $newSpeed = 0;
	private $newHeight = 0;
	private $newWeight = 0;

	//Merge robots in new one
	function mergeRobots($robots)
	{

		foreach ($robots as $type => $quantity) {

			for ($i=0; $i < $quantity; $i++) { 
				$robot = new $type();
				$this->newSpeed = 
					!empty($this->newSpeed)
						?
					min($robot->getSpeed(), $this->newSpeed)
						:
					$robot->getSpeed();
				$this->newHeight += $robot->getHeight();
				$this->newWeight += $robot->getWeight();
			}
		}

		return [
			$this->newSpeed,
			$this->newHeight,
			$this->newWeight
		];

	}

}