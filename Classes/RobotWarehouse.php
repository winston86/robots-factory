<?php

namespace Classes;

class RobotWarehouse {

	private $robots;


	public function getFromWarehouse($type)
	{
		//get all produced robots of type from warehouse
		return $this->robots[$type];
	}

	public function placeToWarehouse($type, $robot, $quantity)
	{
		//place to warehouse
		for ($i=0; $i < $quantity; $i++) { 
			$this->robots[$type][] = $robot;
		}
	}

	public function removeFromWarehouse($robots)
	{
		//remove from warehouse. 
		foreach ($robots as $r => $quantity) {
			for ($i=0; $i < $quantity; $i++) { 
				array_pop($this->robots[$r::$type]);
			}
		}
	}

	public function getCountOnWarehouse($type)
	{
		return count($this->getFromWarehouse($type));
	}

}