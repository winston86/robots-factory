<?php
//Robot of type Robot1
namespace Models;

use Models\Robot;

class Robot1 extends Robot
{
	public static $type = 'Robot1';
	protected $weight = 10;
	protected $speed = 100;
	protected $height = 1000;
}