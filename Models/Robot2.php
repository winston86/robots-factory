<?php

namespace Models;

use Models\Robot;

class Robot2 extends Robot
{
	public static $type = 'Robot2';
	protected $weight = 20;
	protected $speed = 200;
	protected $height = 2000;
}