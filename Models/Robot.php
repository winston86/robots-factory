<?php
//Base Robot Model
namespace Models;

use Structure\RobotStructure as RobotDraft;

/**
 * 
 */
class Robot extends RobotDraft
{
	//Robot property
	public static $type;
	protected $weight;
	protected $speed;
	protected $height;

	//init with properties
	function __construct($properties = []){
		foreach ($properties as $key => $value) {
			if (property_exists(self::class, $key)) {
				$this->$key = $value;
			}
		}
	}

	//Getters

	function getSpeed()
	{
		return $this->speed;
	}
	function getWeight()
	{
		return $this->weight;
	}
	function getHeight()
	{
		return $this->height;
	}

}